const ROWS_PER_PAGE = 10;
const START_PAGE = "start";
const PREV_PAGE = "prev";
const NEXT_PAGE = "next";
const END_PAGE = "end";
const TOTAL_ITEM_START_END = 6;
const TOTAL_ITEM_CENTER = 7;

var dataTable = initDataTable(228);
var totalPages = Math.ceil(dataTable.length / ROWS_PER_PAGE);
var currentIndex = 0;
var endIndex = ROWS_PER_PAGE;
var currentPage = 1;
var rowsOfPage = getRowsOfTable(dataTable, currentIndex, endIndex);

function initDataTable(totalRows){
    var dataTable = [];
    for(var i = 0; i < totalRows; i++){
        dataTable[i] = {
            "no"        : (i+1),
            "city"      : "Quảng Nam",
            "distrist"  : "Huyện Phú Ninh",
            "commune"   : "Xã Tam An",
            "species"   : "Dầu rái, Cao su, Sao xanh" ,
            "volume"    : Math.floor(Math.random() * 100),
            "area"      : Math.floor(Math.random() * 100)
        }
    }
    return dataTable;
}

function getRowsOfTable(dataTable, currentIndex, endIndex){
    var rowsOfPage = [];
    for(var i = 0; i < endIndex - currentIndex; i++){
        rowsOfPage[i] = dataTable[currentIndex + i];
    }
    return rowsOfPage;
}


function showPangination(totalPages, currentPage, TOTAL_ITEM_START_END, TOTAL_ITEM_CENTER){
    console.log(TOTAL_ITEM_CENTER);
    var text = "<a href='#' id='startPage' onclick='clickElementPagination(\"" + START_PAGE + "\")'>&laquo;</a>";
        text += "<a href='#' id='prevPage' onclick='clickElementPagination(\"" + PREV_PAGE + "\")'>&lsaquo;</a>";

    if(currentPage === 1){
        text += "<a href='#' id='1' class='active' onclick='clickElementPagination(1)'>1</a>";
    } else {
        text += "<a href='#' id='1' onclick='clickElementPagination(1)'>1</a>";
    }
             
    if (currentPage < TOTAL_ITEM_START_END) {
        for (var i = 2; i < TOTAL_ITEM_START_END + 2; i++) {
            if (i === TOTAL_ITEM_START_END + 1) {
                text += "<a href='#' id='" + i + "' onclick='clickElementPagination(\"" + NEXT_PAGE + "\")'>...</a>";
            } else {
                text += "<a href='#' id='" + i + "' onclick='clickElementPagination(" + i + ")'>" + i + "</a>";
            }
        }
    } 
    
    if (currentPage <= totalPages - TOTAL_ITEM_START_END + 1 && currentPage >= TOTAL_ITEM_START_END) {
        var temp = Math.floor(TOTAL_ITEM_CENTER / 2);
        for(var i = currentPage - temp; i <= currentPage + temp; i++){

            if (i === currentPage - temp) {
                text += "<a href='#' id='" + i + "' onclick='clickElementPagination(\"" + PREV_PAGE + "\")'>...</a>";
            }
            
            if(i === currentPage + temp){
                text += "<a href='#' id='" + i + "' onclick='clickElementPagination(\"" + NEXT_PAGE + "\")'>...</a>";
            } 

            if(i === currentPage) {
                text += "<a href='#' id='" + i + "' class='active'>" + i + "</a>";
            } 
            
            if(i != currentPage - temp && i != currentPage  + temp && i != currentPage){
                text += "<a href='#' id='" + i + "' onclick='clickElementPagination(" + i + ")'>" + i + "</a>";
            }
        }
    }

    if (currentPage >= totalPages - TOTAL_ITEM_START_END + 2){
        text += "<a href='#'>...</a>";
        for(var i = totalPages - TOTAL_ITEM_START_END + 1; i < totalPages; i++){
            text += "<a href='#' id='" + i + "' onclick='clickElementPagination(" + i + ")'>" + i + "</a>";
        }
    }

    text += "<a href='#' id='" + totalPages + "' onclick='clickElementPagination(" + totalPages + ")'>" + totalPages + "</a>"
         +  "<a href='#' id='nextPage' onclick='clickElementPagination(\"" + NEXT_PAGE + "\")'>&rsaquo;</a>"
         +  "<a href='#' id='endPage' onclick='clickElementPagination(\"" + END_PAGE + "\")'>&raquo;</a>";
    showTextByID("pagination-id", text);
}


function showRowsOfTable(rowsOfPage) {
    var text = "";
    for(var i = 0; i < rowsOfPage.length; i++){
        text += "<tr>"
             +  "   <td class='table-col-1-pl'>" + rowsOfPage[i].no + "</td>"
             +  "   <td>" + rowsOfPage[i].city + "</td>"
             +  "   <td>" + rowsOfPage[i].distrist + "</td>"
             +  "   <td class='commune'>" + rowsOfPage[i].commune + "<i class='fa fa-map' aria-hidden='true'></i></td>"
             +  "   <td>" + rowsOfPage[i].species + "</td>"
             +  "   <td class='text-right'>" + rowsOfPage[i].volume + "</td>"
             +  "   <td class='text-right'>" + rowsOfPage[i].area + "</td>"
             +  "   <td class='text-center'><button class='btn-detail'><i class='fa fa-eye' aria-hidden='true'></i> Chi tiết</button></td>"
             +  "</tr>";
    }
    showTextByID("row-table", text);
}

function changeCurrentAndEndIndexPage(currentPage){
    if(endIndex <= dataTable.length){
        currentIndex = currentPage * ROWS_PER_PAGE - ROWS_PER_PAGE;
    }
    
    if(currentIndex < dataTable.length - ROWS_PER_PAGE){
        endIndex = currentIndex + ROWS_PER_PAGE;
    } else {
        endIndex =  currentIndex + (dataTable.length - currentIndex);
    }
}

function showTextByID(id, text){
    document.getElementById(id).innerHTML = text;
}

function showTableInfo(){
    showTextByID("current-index", currentIndex + 1);
    showTextByID("end-index", endIndex);
    showTextByID("rows-of-page", endIndex - currentIndex);
    showTextByID("num-rows-table", dataTable.length);
}

function changePage(currentIndex, endIndex, dataTable, rowsOfPage){
    rowsOfPage = getRowsOfTable(dataTable, currentIndex, endIndex);
    showRowsOfTable(rowsOfPage);
}

function clickElementPagination(action){
    document.getElementById(currentPage).classList.remove("active");
    switch (action) {
        case START_PAGE:
            currentPage = 1;

            break;
        case PREV_PAGE:
            if (currentPage > 1) {
                currentPage--;
            }
            break;
        case NEXT_PAGE:
            if (currentPage < totalPages) {
                currentPage++;
            }
            break;
        case END_PAGE:
            currentPage = totalPages;
            break;
        default:
            currentPage = action;

    }

    changeCurrentAndEndIndexPage(currentPage);
    changePage(currentIndex, endIndex, dataTable, rowsOfPage);
    showPangination(totalPages, currentPage, TOTAL_ITEM_START_END, TOTAL_ITEM_CENTER);
    document.getElementById(currentPage).classList.add("active");
    showTableInfo();
}

showRowsOfTable(rowsOfPage);
showPangination(totalPages,currentPage, TOTAL_ITEM_START_END, TOTAL_ITEM_CENTER);
showTableInfo();